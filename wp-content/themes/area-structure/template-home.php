<?php 
	/*Template name: Home */
    get_header();
?>
    <div class="slide-row">
        <div class="slider-home">
            <?= wp_is_mobile()==false ? do_shortcode('[rev_slider alias="slider-home"][/rev_slider]'):do_shortcode('[rev_slider alias="home-mobile"][/rev_slider]')?>
        </div>
    </div>
    <div class="row-linsul">
        <div class="left-image">
            <img src="<?= get_image_url("banner-lateral-linsul.webp"); ?>" alt="Banner Linsul">
        </div>
        <div class="right-text">
            <?= get_field("banner"); ?>
        </div>
    </div>
    <div class="row-cards">
        <?php foreach(get_field("diretrizes") as $valor): ?>
            <div class="card">
                <div class="top">
                    <div class="img">
                        <img src="<?= $valor['imagem']['url'] ?>" alt="Ícone">
                    </div>
                    <div class="title">
                        <?= $valor['titulo'] ?>
                    </div>
                </div>
                <div class="bottom">
                    <div class="text">
                        <?= $valor['texto'] ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row-produtos" style="background-image: url('<?= get_image_url('fundo.webp')?>')">
        <div class="content">
            <h2 class="produto-title">produtos<br> em destaque</h2>
            <div class="produtos">
                <div class="al-container">  
                    <ul id="produtos-slider">
                        <?php
                        $produtos = new WP_Query(array(
                            'post_type' => 'produtos',
                            'posts_per_page'	=> -1,
                            'meta_key' => 'destaque',
                            'meta_query' => array(
                                array(
                                    'key'     => 'destaque',
                                    'value'   => array( "Sim" ),
                                    'compare' => 'IN'
                                )
                            )
                            ));
                            $cont=0;
                            while($produtos->have_posts()):
                                $cont++;
                                $produtos->the_post();
                                ?>   
                                <li>
                                    <div id="<?= $cont?>" class="produto" style="background-image: url('<?= get_image_url("bottom-border.png")?>');">  
                                        <a href="<?= get_page_link() ?>">
                                            <section>
                                                <div class="img-produto">
                                                    <img src="<?= get_field('galeria_thumbnail')[0]['url']?>" alt="">
                                                </div>
                                                <div class="texto-produto">
                                                    <h3><?= get_the_title() ?></h3>
                                                </div>
                                            </section>
                                        </a>
                                    </div>
                                </li>
                            <?php
                                endwhile;
                            ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row-produtos-mobile" style="background-image: url('<?= get_image_url('fundo.webp')?>')">
        <div class="content">
            <h2 class="produto-title">produtos<br> em destaque</h2>
            <div class="produtos">
                <div class="al-container">
                    <ul id="produtos-mobile">
                        <?php
                        $produtos = new WP_Query(array(
                            'post_type' => 'produtos',
                            'posts_per_page'	=> -1,
                            'meta_key' => 'destaque',
                            'meta_query' => array(
                                array(
                                    'key'     => 'destaque',
                                    'value'   => array( "Sim" ),
                                    'compare' => 'IN'
                                )
                            )
                            ));
                            $cont=0;
                            while($produtos->have_posts()):
                                $cont++;
                                $produtos->the_post();
                                ?>   
                                <li>
                                    <div id="<?= $cont?>" class="produto" style="background-image: url('<?= get_image_url("bottom-border.png")?>');">  
                                        <a href="<?= get_page_link() ?>">
                                            <section>
                                                <div class="img-produto">
                                                    <img src="<?= get_field('galeria_thumbnail')[0]['url']?>" alt="">
                                                </div>
                                                <div class="texto-produto">
                                                    <h3><?= get_the_title() ?></h3>
                                                </div>
                                            </section>
                                        </a>
                                    </div>
                                </li>
                            <?php
                                endwhile;
                            ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row-clientes">
        <div class="al-container">
            <h2 class="title">nossos clientes</h2>
            <div class="clientes">
                <ul id="clientes">
                    <?php  
                        $clientes = new WP_Query(array(
                            'post_type' => 'clientes',
                            'posts_per_page'	=> -1
                        ));
                        $cont=0;
                        while($clientes->have_posts()):
                            $cont++;
                            $clientes->the_post();
                            ?>   
                            <li>
                                <div id="<?= $cont?>" class="cliente">
                                    <img src="<?= get_field('logo_cliente')['url']?>" alt="">
                                </div>
                            </li>
                        <?php
                            endwhile;
                    ?>
                </ul>
            </div>
            <div class="clientes-mobile">
                <ul id="clientes-mobile">
                    <?php  
                        $clientes = new WP_Query(array(
                            'post_type' => 'clientes',
                            'posts_per_page'	=> -1
                        ));
                        $cont=0;
                        while($clientes->have_posts()):
                            $cont++;
                            $clientes->the_post();
                            ?>   
                            <li>
                                <div id="<?= $cont?>" class="cliente">
                                    <img src="<?= get_field('logo_cliente')['url']?>" alt="">
                                </div>
                            </li>
                        <?php
                            endwhile;
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="row-download" id="row-download" style="background-image: url('<?= get_image_url('lateral.webp') ?>')">  
        <div class="right" style="background-image: url('<?= get_image_url('lateral.webp') ?>')">
            <div class="left">
                <img src="<?= get_image_url('catalogo.png') ?>">
            </div>
            <div class="half-container">
                <h3>faça o download do nosso catálogo</h3>
                <p>Conheça toda a gama de soluções inovadoras para tratamento de efluentes que a Linsul oferece!</p>
                <div class="form">
                    <?= do_shortcode('[contact-form-7 id="278" title="Download"]') ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>
