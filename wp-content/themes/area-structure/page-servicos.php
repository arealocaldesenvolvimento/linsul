<?php get_header() ?>

    <div class="row-servico">
        <div class="al-container">
            <h1 class="title">Serviços</h1>
            <div class="contents">
            <?php
            foreach(get_field("servicos") as $chave => $valor):  
            ?>
                <div class="row-content">
                    <?php if($chave%2===0){ ?>
                        <div class="content-left texto">
                            <h2 class="subtitle"><?= $valor['titulo'] ?></h2>
                            <div class="legend"><?= $valor['legenda'] ?></div>
                        </div>
                        <div class="content-right img">
                            <img src="<?= $valor['imagem']['url'] ?>" alt="<?= $valor['imagem']['title'] ?>">
                        </div>
                    <?php }else{ ?>
                        <div class="content-left img">
                            <img src="<?= $valor['imagem']['url'] ?>" alt="<?= $valor['imagem']['title'] ?>">
                        </div>
                        <div class="content-right texto">
                            <h2 class="subtitle"><?= $valor['titulo'] ?></h2>
                            <div class="legend"><?= $valor['legenda'] ?></div>
                        </div>
                    <?php } ?>
                </div>
            <?php 
            endforeach;
            ?>
            </div>
        </div>
    </div>

<?php get_footer() ?>
