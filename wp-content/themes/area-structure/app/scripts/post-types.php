<?php

flush_rewrite_rules();

/**
 * Example post-type.
 */

function type_post_produtos() {
    $labels = array(
        'name' => _x('Produtos', 'post type general name'),
        'singular_name' => _x('Produto', 'post type singular name'),
        'add_new' => _x('Adicionar Novo', 'Novo item'),
        'add_new_item' => __('Novo Produto'),
        'edit_item' => __('Editar Produto'),
        'new_item' => __('Novo Produto'),
        'view_item' => __('Ver Produto'),
        'search_items' => __('Procurar Produtos'),
        'not_found' =>  __('Nenhum Produto encontrado'),
        'not_found_in_trash' => __('Nenhum Produto encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Produtos'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-cart',
        'supports' => array('title','editor','thumbnail','custom-fields', 'revisions')
    );

    register_post_type( 'produtos' , $args );
}
add_action('init', 'type_post_produtos');

function type_post_clientes() {
    $labels = array(
        'name' => _x('Clientes', 'post type general name'),
        'singular_name' => _x('Cliente', 'post type singular name'),
        'add_new' => _x('Adicionar Novo', 'Novo item'),
        'add_new_item' => __('Novo Cliente'),
        'edit_item' => __('Editar Cliente'),
        'new_item' => __('Novo Cliente'),
        'view_item' => __('Ver Cliente'),
        'search_items' => __('Procurar Clientes'),
        'not_found' =>  __('Nenhum Cliente encontrado'),
        'not_found_in_trash' => __('Nenhum Cliente encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Clientes'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-admin-users',
        'supports' => array('title','editor','thumbnail','custom-fields', 'revisions')
    );

    register_post_type( 'clientes' , $args );
}
add_action('init', 'type_post_clientes');

function type_post_aplicacoes() {
    $labels = array(
        'name' => _x('Aplicações', 'post type general name'),
        'singular_name' => _x('Aplicação', 'post type singular name'),
        'add_new' => _x('Adicionar Novo', 'Novo item'),
        'add_new_item' => __('Novo Aplicação'),
        'edit_item' => __('Editar Aplicação'),
        'new_item' => __('Novo Aplicação'),
        'view_item' => __('Ver Aplicação'),
        'search_items' => __('Procurar Aplicações'),
        'not_found' =>  __('Nenhum Aplicação encontrado'),
        'not_found_in_trash' => __('Nenhum Aplicação encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Aplicações'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-cart',
        'supports' => array('title','editor','thumbnail','custom-fields', 'revisions')
    );

    register_post_type( 'aplicacoes' , $args );
}
add_action('init', 'type_post_aplicacoes');

// function type_post_noticias() {
//     $labels = array(
//         'name' => _x('Notícias', 'post type general name'),
//         'singular_name' => _x('Notícia', 'post type singular name'),
//         'add_new' => _x('Adicionar Novo', 'Novo item'),
//         'add_new_item' => __('Nova Notícia'),
//         'edit_item' => __('Editar Notícia'),
//         'new_item' => __('Novo Aplicação'),
//         'view_item' => __('Ver Notícia'),
//         'search_items' => __('Procurar Notícia'),
//         'not_found' =>  __('Nenhuma Notícia encontrada'),
//         'not_found_in_trash' => __('Nenhuma Notícia encontrada na lixeira'),
//         'parent_item_colon' => '',
//         'menu_name' => 'Notícias'
//     );

//     $args = array(
//         'labels' => $labels,
//         'public' => true,
//         'public_queryable' => true,
//         'show_ui' => true,
//         'query_var' => true,
//         'rewrite' => true,
//         'capability_type' => 'post',
//         'has_archive' => true,
//         'hierarchical' => false,
//         'menu_position' => null,
//         'menu_position' => 4,
//         'menu_icon' => 'dashicons-format-aside',
//         'supports' => array('title','editor','thumbnail','custom-fields', 'revisions')
//     );

//     register_post_type( 'noticias' , $args );
// }
// add_action('init', 'type_post_noticias');