<?php

/**
 * Imports.
 */
include_once ABSPATH.'/wp-load.php';

/**
 * Fetch all states.
 */
function getCursos(): WP_REST_Response
{
    try {

        $estados = [];
        $cidades = [];
        $cursos_name = [];
        $cursos_slug = [];
        $polos = [];
        $modalidade = [];
        $permalink = [];

        $meta_query = array('relation'=>'AND');

        if(!empty($_GET['estado'])){
            array_push($meta_query, array(
                'key'	  	=> 'estado',
                'value'	  	=> $_GET['estado'],
                'compare' 	=> '=',
            ));
        }

        if(!empty($_GET['cidade'])){
            array_push($meta_query, array(
                'key'	  	=> 'cidade',
                'value'	  	=> $_GET['cidade'],
                'compare' 	=> 'LIKE',
            ));
        }

        if(!empty($_GET['tipo'])){
            array_push($meta_query, array(
                'key'	  	=> 'grau',
                'value'	  	=> $_GET['tipo'],
                'compare' 	=> 'LIKE',
            ));
        }
        
        $curso_id = null;

        if(!empty($_GET['curso'])){
            $curso_id = explode(',', $_GET['curso']);
        }

        if(!empty($_GET['polo'])){
            $polo = array (
                'taxonomy' => 'polos',
                'field' => 'term_id',
                'terms' => $_GET['polo'],
            );
        }

        if(!empty($_GET['modalidade'])){
            array_push($meta_query, array(
                'key'	  	=> 'modalidade',
                'value'	  	=> $_GET['modalidade'],
                'compare' 	=> '=',
            ));
        }

        $cursos = new WP_Query(array(
            'post_type' => 'cursos',
            'posts_per_page'=> -1,
            'orderby' => 'date',
            'order' => 'desc',
            'tax_query' => array($polo),
            'meta_query'	=> $meta_query,
            'post__in' => $curso_id
        ));
        while($cursos->have_posts()) : $cursos->the_post(); 
            if(!in_array(get_field('estado'), $estados)):
                $estados[] = get_field('estado');
            endif;
            if(!in_array(get_field('cidade'), $cidades)):
                $cidades[] = get_field('cidade');
            endif;
            if(!in_array(get_the_title(), $cursos_name)):
                $cursos_name[get_the_ID()] = get_the_title();
                $cursos_slug[get_the_ID()] = get_the_permalink();
            else:
                $oldkey = array_search(get_the_title(), $cursos_name);
                $cursos_name[$oldkey.','.get_the_ID()] = get_the_title();
                $cursos_slug[$oldkey.','.get_the_ID()] = get_the_permalink();
                unset($cursos_name[$oldkey]);
                unset($cursos_slug[$oldkey]);
            endif;
            foreach (get_the_terms(get_the_ID(), 'polos') as &$value) {
                if(!in_array($value->name, $polos)):
                    $polos[$value->term_id] = $value->name;
                endif;
            }
            if(!in_array(get_field('modalidade'), $modalidade)):
                $modalidade[] = get_field('modalidade');
            endif;
            $permalink = get_the_permalink();
        endwhile; 
        wp_reset_postdata();
        if ($cursos->have_posts()) {
            return new WP_REST_Response([
                'status' => 200,
                'estados' => $estados,
                'cidades' => $cidades,
                'cursos' => $cursos_name,
                'cursos_slug' => $cursos_slug,
                'polos' => $polos,
                'modalidades' => $modalidade,
                'link' => $permalink,
                'message' => 'Cursos obtidos com sucesso',
            ]);
        }

        return new WP_REST_Response([
            'status' => 500,
            'message' => 'Erro ao obter os cursos',
        ]);
    } catch (\Throwable $th) {
        return new WP_REST_Response([
            'status' => 500,
            'message' => 'Um erro inesperado ocorreu: '.$th->getMessage(),
        ]);
    }
}
