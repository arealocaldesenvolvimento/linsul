<?php get_header() ?>
    <div class="content-aplicacoes">
        <div class="al-container">
            <h1 class="title">Aplicações</h1>
        </div>
        <div class="al-container aplicacao-div">
            <div class="lista">
                <ul class="aplicacoes">
                    <?php 
                        $atual=get_the_title();
                        $aplicacoes = new WP_Query(array(
                            'post_type' => 'aplicacoes',
                            'posts_per_page'	=> -1
                        ));
                        while($aplicacoes->have_posts()):
                            $aplicacoes->the_post();
                        ?>
                        <li <?= get_the_title()== $atual? 'class="active"': ""?>>
                            <a href="<?= get_home_url()."/aplicacoes/".get_page_uri() ?>"><?= get_the_title() ?></a>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
            <div class="lista-mobile">
                <select id="lista-aplicacoes">
                    <?php 
                        $aplicacoes = new WP_Query(array(
                            'post_type' => 'aplicacoes',
                            'posts_per_page'	=> -1
                        ));
                        while($aplicacoes->have_posts()):
                            $aplicacoes->the_post();
                        ?>
                        <option value="<?= get_page_uri() ?>" <?= get_the_title()== $atual? 'selected': ""?>>
                            <?= get_the_title() ?></a>
                            </option>
                    <?php endwhile; ?>
                </select>
            </div>
            <div class="aplicacao">
                <?php 
                    $aplicacaoAtual = new WP_Query(array(
                        'post_type' => 'aplicacao',
                        'posts_per_page'	=> 1,
                        'meta_key' => 'post_title',
                        'meta_query' => array(
                            array(
                                'key'     => 'post_title',
                                'value'   => array( $atual),
                                'compare' => 'IN'
                            )
                        )
                        ));
                        $aplicacaoAtual->the_post();
                ?>
                <h2 class="subtitle"><?= $atual ?></h2>
                <div class="aplicacao-galeria">
                    <?php
                        if(get_field("fotos")[0]['url']!=null){    
                            ?>
                            <div class="slide-show">
                                <ul id="lightSlider" class="slider-aplicacao">
                                    <?php
                                    if(get_field('fotos')):
                                        foreach(get_field('fotos') as $imagem):
                                            ?>
                                            <li data-thumb="<?= $imagem['url'] ?>" data-src="<?= $imagem['url'] ?>">
                                                <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>" height="420px"> 
                                            </li>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                    ?>
                </div>
                <div class="topicos">
                    <h2 class="subtitle">Funcionamento</h2>
                    <div class="text"><?= get_field('funcionamento')?></div>
                </div>
                <!-- <div class="topicos">
                    <h2 class="subtitle">Aplicações</h2>
                    <div class="lista">
                        <ul>
                            <?php foreach(get_field("aplicacoes") as $aplicacao): ?>
                                <li><?= $aplicacao['aplicacao']?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="topicos">
                    <h2 class="subtitle">Vantagens</h2>
                    <div class="lista">
                        <ul>
                            <?php foreach(get_field("vantagens") as $vantagem): ?>
                                <li><?= $vantagem['vantagem']?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="topicos">
                    <h2 class="subtitle">Capacidades Disponiveis</h2>
                    <div class="lista">
                        <ul>
                            <?php foreach(get_field("capacidades") as $capacidade): ?>
                                <li><?= $capacidade['capacidade']?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div> -->
                <div class="row-button">
                    <a onclick='$("#modal").show();$("#modal #produtos").val($(".content-aplicacoes .aplicacao-div .aplicacao .subtitle").html());'>SOLICITAR ORÇAMENTO</a>
                </div>
                <?php if(get_field('videos')[0]!=null){?>
                    <div class="videos">
                        <h2 class="subtitle">Vídeos</h2>
                        <a class="prevVideosAplicacao"><img class="prev" src="<?= get_image_url('prev.png')?>" alt="Ícone Seta Anterior"></a>
                        <a class="nextVideosAplicacao"><img class="next" src="<?= get_image_url('next.png')?>" alt="Ícone Seta Posterior"></a>
                        <div class="limitador">
                            <ul class="ul-videos-aplicacao">
                            <?php 
                                foreach(get_field('videos') as $video):
                                    ?>
                                        <li><div><?= $video['iframe']?><div></li>
                                    <?php
                                endforeach;
                            ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
                <?php if(get_field('videos')[0]!=null){?>
                <div class="videos-mobile">
                    <h2 class="subtitle">Vídeos</h2>
                    <div class="mobile">
                        <a class="prevVideosAplicacaoMobile"><img class="prev" src="<?= get_image_url('prev.png')?>" alt="Ícone Seta Anterior"></a>
                        <div class="limitador">
                            <ul class="ul-videos-aplicacao-mobile">
                            <?php 
                                foreach(get_field('videos') as $video):
                                    ?>
                                        <li><div><?= $video['iframe']?><div></li>
                                    <?php
                                endforeach;
                            ?>
                            </ul>
                        </div>
                        <a class="nextVideosAplicacaoMobile"><img class="next" src="<?= get_image_url('next.png')?>" alt="Ícone Seta Posterior"></a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php get_footer() ?>
<div id="modal" style="display: none">
    <div class="sombra" onclick="$('#modal').hide();"></div>
    <div class="modal">
        <div class="pop-up">
            <div class="contain">
                <div class="close" onclick="$('#modal').hide();">
                    <i class="fa-solid fa-xmark"></i>
                </div>
            </div>
            <?= do_shortcode('[contact-form-7 id="1174" title="Orçamento"]'); ?>
        </div>
    </div>
</div>