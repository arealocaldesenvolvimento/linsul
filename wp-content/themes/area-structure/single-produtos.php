<?php get_header() ?>
    <div class="row-produto">
        <div class="al-container">
            <div class="apresentacao">
                <h1 class="title"><?= get_the_title()?></h1>
                <?php
if (get_field("galeria_thumbnail")[0]['url'] != null) {
?>
    <div class="slide-show">
        <ul id="lightSlider" class="slider-produtos">
        <?php
if (get_field('galeria_thumbnail')) :
    foreach (get_field('galeria_thumbnail') as $imagem) :
        ?>
        <li data-thumb="<?= $imagem['url'] ?>" data-src="<?= $imagem['url'] ?>">
            <a href="#" onclick="openPopup('<?= $imagem['url'] ?>', '<?= $imagem['title'] ?>'); return false;">
                <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>" height="420px">
            </a>
        </li>
        <?php
            endforeach;
        endif;
        ?>
        </ul>
        </div>
        <div id="popup" style="display: none;">
            <div id="popup-content">
                <span id="popup-close" onclick="closePopup()">&times;</span>
                <img id="popup-image" src="" alt="">
            </div>
        </div>
        <script>
            function openPopup(url, title) {
                var popup = document.getElementById('popup');
                var popupImage = document.getElementById('popup-image');
                popupImage.src = url;
                popupImage.alt = title;
                popup.style.display = 'flex';
            }

            function closePopup() {
                var popup = document.getElementById('popup');
                popup.style.display = 'none';
            }
        </script>

        <?php
        }
        ?>
                <div class="text">
                    <?= get_field('descricao')?>
                </div>
            </div>
        </div>
    </div>
    <div class="row-desc-produto">
        <div class="al-container">
            <?php if(get_field("caracteristicas_construtivas")!=null){ ?>
                <div class="desc">
                    <h2 class="subtitle">Aplicações</h2>
                    <ul class="list">
                        <?php foreach(get_field("caracteristicas_construtivas") as $capacidade): ?>
                            <li><?= $capacidade['caracteristica']?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php } ?>
            <?php if(get_field("vantagens")!=null){ ?>
                <div class="desc">
                    <h2 class="subtitle">Vantagens</h2>
                    <ul class="list">
                        <?php foreach(get_field("vantagens") as $vantagem): ?>
                            <li><?= $vantagem['vantagem']?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php } ?>
            <?php if(get_field("capacidades")!=null){ ?>
                <div class="desc">
                    <h2 class="subtitle">capacidades disponíveis</h2>
                    <ul class="list-null-style">
                        <?php foreach(get_field("capacidades") as $capacidade): ?>
                            <li><?= $capacidade['capacidade']?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="row-button">
    <a onclick='$("#modal").show();$("#modal #produtos").val($(".row-produto .al-container .apresentacao .title").html());'>SOLICITAR ORÇAMENTO</a>
</div>

<?php if (get_field('fotos')[0] != null || get_field('videos')[0] != null) { ?>
    <div class="hr">
        <hr>
    </div>
<?php } ?>

<?php if (get_field('fotos')[0] != null) { ?>
    <div class="row-fotos">
        <a class="prevFotos"><img class="prev" src="<?= get_image_url('prev.png') ?>" alt="Ícone Seta Anterior"></a>
        <div class="al-container">
            <h2 class="subtitle">Fotos</h2>
            <ul class="ul-fotos">
                <?php foreach (get_field('fotos') as $fotos) : ?>
                    <li data-thumb="<?= $fotos['url'] ?>" data-src="<?= $fotos['url'] ?>">
                        <a href="#" onclick="openPopup('<?= $fotos['url'] ?>', '<?= $fotos['title'] ?>'); return false;">
                            <img src="<?= $fotos['url'] ?>" alt="<?= $fotos['title'] ?>">
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <a class="nextFotos"><img class="next" src="<?= get_image_url('next.png') ?>" alt="Ícone Seta Posterior"></a>
    </div>
<?php } ?>

<div id="popup" style="display: none;">
    <div id="popup-content">
        <span id="popup-close" onclick="closePopup()">&times;</span>
        <img id="popup-image" src="" alt="">
    </div>
</div>

<script>
    function openPopup(url, title) {
        var popup = document.getElementById('popup');
        var popupImage = document.getElementById('popup-image');
        popupImage.src = url;
        popupImage.alt = title;
        popup.style.display = 'flex';
    }

    function closePopup() {
        var popup = document.getElementById('popup');
        popup.style.display = 'none';
    }
</script>

<div class="mobile">
    <?php if (get_field('fotos')[0] != null) { ?>
        <div class="row-fotos-mobile">
            <div class="al-container">
                <h2 class="subtitle">Fotos</h2>
                <ul class="ul-fotos-mobile">
                    <?php foreach (get_field('fotos') as $fotos) : ?>
                        <li data-thumb="<?= $fotos['url'] ?>" data-src="<?= $fotos['url'] ?>">
                            <a href="#" onclick="openPopup('<?= $fotos['url'] ?>', '<?= $fotos['title'] ?>'); return false;">
                                <img src="<?= $fotos['url'] ?>" alt="<?= $fotos['title'] ?>">
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php } ?>
</div>

        <?php if(get_field('videos')[0]!=null){?>
            <div class="row-videos-mobile">
                <div class="al-container">
                    <h2 class="subtitle">Vídeos</h2>
                    <ul class="ul-videos-mobile">
                        <?php 
                            foreach(get_field('videos') as $video):
                                ?>
                                    <li><div><?= $video['iframe']?><div></li>
                                <?php
                            endforeach;
                        ?>
                    </ul>
                </div>
            </div>
        <?php }?>
    </div>
<?php get_footer() ?>
<div id="modal" style="display: none">
    <div class="sombra" onclick="$('#modal').hide();"></div>
    <div class="modal">
        <div class="pop-up">
            <div class="contain">
                <div class="close" onclick="$('#modal').hide();">
                    <i class="fa-solid fa-xmark"></i>
                </div>
            </div>
            <?= do_shortcode('[contact-form-7 id="1174" title="Orçamento"]'); ?>
        </div>
    </div>
</div>