<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/style.css"
    >
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightslider.css">
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightgallery.css">
    <title>
        <?php echo is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
    </title>
    <?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3X9CX9P3JH"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-3X9CX9P3JH');
    </script>
	
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WHRV277S');</script>
	<!-- End Google Tag Manager -->

</head>
<body <?php body_class(); ?>>
    <!-- Header -->
        <div class="sup-menu">
            <div class="al-container">
                <div class="contact-icons">
                    <div>
                        <a class="icon" target="_blank" href="<?= get_field('instagram', 'option')?>">
                            <img src="<?= get_image_url("instagram-icon.png") ?>">
                        </a>
                    </div>
                    <div>
                        <a class="icon" target="_blank" href="<?= get_field('facebook', 'option')?>">
                            <img src="<?= get_image_url("facebook-icon.png") ?>">
                        </a>
                    </div>
                    <div>
                        <a class="icon" target="_blank" href="<?= get_field('linkedin', 'option')?>">
                            <img src="<?= get_image_url("linkedin-icon.png") ?>">
                        </a>
                    </div>
                    <div>
                        <a class="icon" target="_blank" href="<?= get_field('youtube', 'option')?>">
                            <img src="<?= get_image_url("youtube-icon.png") ?>">
                        </a>
                    </div>
                    <div>
                        <a class="icon" target="_blank" href="tel: <?= get_field('telefone', 'option')['back']?>">
                            <img src="<?= get_image_url("phone-white.png") ?>">
                        </a>
                    </div>
                    <div>
                        <a class="icon" target="_blank" href="mailto: <?= get_field('email', 'option')?>">
                            <img src="<?= get_image_url("mail-white.png") ?>">
                        </a>
                    </div>
                </div>
                <?php if(!wp_is_mobile()){ ?>
                    <div class="pais">
                        <?= do_shortcode('[gtranslate]') ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <header role="heading" class="main-header">
        <div class="al-container">
            <div class="sub-menu">
                <div class="logo">
                    <a title="Linsul" href="<?= get_site_url() ?>">
                        <img src="<?= get_field('logomarca', 'option')['url'] ?>" alt="Logo Linsul">
                    </a>
                </div>
                <div class="main-menu">
                    <?php wp_nav_menu(array('menu'=> 'principal', 'theme_location' => 'principal', 'container' => false)); ?>
                </div>
            </div>
        </div>
    </header>
    <header class="mobile-menu">
        <div class="mob-icon">
            <i class="fa-solid fa-bars"></i>
        </div>
        <div class="logo">
            <a title="Linsul" href="<?= get_site_url() ?>">
                <img src="<?= get_field('logomarca', 'option')['url'] ?>" alt="Logo Linsul">
            </a>
        </div>
        <?php if(wp_is_mobile()){ ?>
            <div class="pais">
                <?= do_shortcode('[gtranslate]') ?>
            </div>
        <?php } ?>
    </header>
    <div class="mob-modal">
        <div class="sombra"></div>
        <div class="menu">
            <div class="close">
                <i class="fa-solid fa-xmark"></i>
            </div>
            <?php wp_nav_menu(array('menu'=> 'principal', 'theme_location' => 'principal', 'container' => false)); ?>
        </div>
    </div>
    <!-- Wrapper -->
    <div id="wrapper">
    <div class="whatsapp-link">
        <a href="https://api.whatsapp.com/send?phone=55<?= get_field('whatsapp', 'option')['back']?>" target="_blank"><img src="<?= get_image_url("whatsapp-lateral.png") ?>"></a>
    </div>
    