<?php get_header() ?>

    <div class="al-container">
        <div class="center-error-text">
            <h2 class="title">Erro 404</h2><br>
            <div class="text">
                <p>
                    A página não foi encontrada, faça uma busca no site ou entre em contato conosco.
                </p>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        document.getElementById('s') && document.getElementById('s').focus()
    </script>

<?php get_footer() ?>
