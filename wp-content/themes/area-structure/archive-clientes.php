<?php get_header() ?>
    <div class="all-clientes">
        <div class="row-clientes">
            <div class="al-container">
                <h2 class="title">clientes</h2>
                <div class="clientes">
                <?php
                    $clientes = new WP_Query(array(
                        'post_type' => 'clientes',
                        'posts_per_page'	=> -1
                    ));
                    $cont=0;
                    while($clientes->have_posts()):
                        $cont++;
                        $clientes->the_post();
                        ?>   
                        <div id="<?= $cont?>" class="cliente">
                            <a href="<?= get_field('link')?>" target="_blank"><img src="<?= get_field('logo_cliente')['url']?>" alt="<?= get_field('logo_cliente')['title']?>"></a>
                        </div>
                    <?php
                        endwhile;
                    ?>
                </div>
            </div>
        </div> 
</div>
<?php get_footer() ?>