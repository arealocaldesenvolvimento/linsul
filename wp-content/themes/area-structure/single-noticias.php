<?php get_header() ?>
    <div class="row-single-noticia">
        <div class="al-container">
            <div class="img-noticia">
                <img src="<?= get_field('imagem')['url'] ?>" alt="<?= get_field('imagem')['imagem']['title'] ?>">
            </div>
            <div class="texto-noticia">
                <h2 class="subtitle"><?= get_the_title(); ?></h2>
                <h2 class="publicado"><?= get_the_date(); ?></h2>
                <div class="legend"><?= get_field('texto') ?></div>
            </div> 
        </div>                   
    </div>
<?php get_footer() ?>