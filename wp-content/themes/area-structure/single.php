<?php get_header() ?>
    <div class="row-single-noticia">
        <div class="al-container">
            <div class="img-noticia">
                <img src="<?= get_field('imagem')['url'] ?>" alt="<?= get_field('imagem')['imagem']['title'] ?>">
            </div>
            <div class="texto-noticia">
                <h2 class="subtitle"><?= get_the_title(); ?></h2>
                <h2 class="publicado"><?= get_the_date(); ?></h2>
                <div class="legend"><?= get_field('texto') ?></div>
            </div> 
        </div>                   
    </div>
    <div class="al-container">
        <div class="compartilhe">
            <span>Compartilhe</span>
            <div class="links">
                <a id="copy" link="<?= get_permalink() ?>">
                    <img src="<?= get_image_url("anchor.png") ?>" alt="Ícone de link">
                </a>
                <?= do_shortcode('[addtoany]') ?>
            </div>
        </div>
    </div>
<?php get_footer() ?>