<?php get_header() ?>
    <div class="all-aplicacoes">
        <div class="al-container">
            <h1 class="title">Aplicações</h1>
            <div class="row-aplicacoes">
                <div class="content">
                    <div class="aplicacoes">
                        <div class="al-container">
                            <?php
                            $aplicacao = new WP_Query(array(
                                'post_type' => 'aplicacoes',
                                'posts_per_page'	=> -1,
                            ));
                            $cont=0;
                            while($aplicacao->have_posts()):
                                $cont++;
                                $aplicacao->the_post();
                                ?>   
                                <div id="<?= $cont?>" class="aplicacao" style="background-image: url('<?= get_image_url("bottom-border.png")?>');">        
                                    <a href="<?= get_permalink() ?>">
                                        <section>
                                            <div class="img-aplicacao">
                                                <img src="<?= get_field('fotos')[0]['url']?>" alt="">
                                            </div>
                                            <div class="texto-aplicacao">
                                                <h3><?= get_the_title() ?></h3>
                                            </div>
                                        </section>
                                    </a>
                                </div>
                            <?php
                                endwhile;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>