<?php get_header() ?>
    <div class="row-noticia">
        <div class="al-container">
            <h1 class="title">Notícias</h1>
            <div class="contents">
                <?php
                $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                $noticias = new WP_Query(array(
                    'post_type' => 'post',
                    'posts_per_page'	=> 4,
                    'paged' => $paged
                ));
                $cont=0;
                while($noticias->have_posts()): 
                $noticias->the_post();
                ?>
                    <a href="<?= get_page_link() ?>">
                        <div class="row-content">
                            
                            <div class="content-left texto">
                                <h2 class="subtitle"><?= get_the_title(); ?></h2>
                                <h2 class="publicado"><?= get_the_date(); ?></h2>
                                <div class="legend"><?= strval(get_field('texto')) ?></div>
                            </div>
                            <div class="content-left img">
                                <img src="<?= get_field('imagem')['url'] ?>" alt="<?= get_field('imagem')['imagem']['title'] ?>">
                            </div>
                        </div>
                    </a>
                <?php  
                $cont++;
                endwhile;
                ?>
            </div>
            <?php if($noticias->max_num_pages>=2) {?>
            <div class="bottom clearfix centered-button">
                <nav class="load_more ler-mais">
                    <?php echo get_next_posts_link('Carregar mais!', $noticias->max_num_pages); ?>
                </nav>
            </div>
            <?php } ?>
        </div>
    </div>

<?php get_footer() ?>
