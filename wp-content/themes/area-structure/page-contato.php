<?php get_header() ?>
    <div class="row-contato">
        <div class="al-container">
            <h1 class="title">Contato</h1>
            <div class="row-contatos">
                <div class="contato">
                    <img src="<?= get_image_url('phone-2.png'); ?>" alt="Phone icon" id="phone">
                    <a class="link" href="tel: <?= get_field('telefone', 'option')['back']?>">
                        <?= get_field('telefone', 'option')['front']?>
                    </a>
                </div>
                <div class="contato">
                    <img src="<?= get_image_url('mail-2.png'); ?>" alt="E-mail icon" id="mail">
                    <a class="link" href="mailto:<?= get_field('email', 'option')?>"><?= get_field('email', 'option')?></a>
                </div>
                <div class="contato">
                    <img src="<?= get_image_url('whatsapp.png'); ?>" alt="Whatsapp icon" id="whatsapp">
                    <a class="link" target="_blank" href="https://api.whatsapp.com/send?phone=55<?= get_field('whatsapp', 'option')['back']?>">
                        <?= get_field('whatsapp', 'option')['front']?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="hr"><hr></div>
    <div class="row-form">
        <div class="al-container">
            <div class="text">
                Será um prazer ajudar e esclarecer todas as suas solicitações e dúvidas. Envie suas sugestões ou críticas para que a Linsul possa evoluir em seus produtos e atendimento.
                <br><br>
                Fale com a nossa equipe através do formulário abaixo:
            </div>
            <div class="form">
                <?= do_shortcode('[contact-form-7 id="228" title="contato"]'); ?>
            </div>
        </div>
    </div>
    <div class="hr"><hr></div>
    <div class="row-localizacao">
        <div class="al-container">
            <img src="<?= get_image_url('map-2.png'); ?>" alt="Map icon" id="map">
            <a target="_blank" class="link" href="<?= get_field('link_de_endereco', 'option')?>">Rodovia SC-110 nº 400, Margem Esquerda - Urbano, CEP 89.182-000, Lontras – SC
        </div>
    </div>
    <div class="map">
        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3548.445387241278!2d-49.644784884948685!3d-27.205162983002502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e3e979a7acd165%3A0xe2a58fc7aba93430!2sLinsul%20Solu%C3%A7%C3%B5es%20Ambientais%20Ltda!5e0!3m2!1spt-BR!2sbr!4v1644600444007!5m2!1spt-BR!2sbr" 
            width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe> -->

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56795.523078130806!2d-49.620717068749975!3d-27.165092599999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e3e979a7acd165%3A0xe2a58fc7aba93430!2sLinsul%20Ind%C3%BAstria%20e%20Solu%C3%A7%C3%B5es%20Ambientais%20Ltda!5e0!3m2!1spt-BR!2sbr!4v1678192516044!5m2!1spt-BR!2sbr" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
<?php get_footer() ?>