<?php get_header() ?>
    <div class="row-trabalhe-conosco">
        <div class="al-container">
            <h1 class="title">Trabalhe conosco</h1>
            <div class="form">
                <?= do_shortcode('[contact-form-7 id="224" title="trabalho-contato"]'); ?>
            </div>
        </div>
    </div>
<?php get_footer() ?>