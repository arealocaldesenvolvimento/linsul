/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}

/**
 * Contact form 7 alerts
 */
const form = document.querySelector('.wpcf7')
if (form) {
    form.addEventListener('wpcf7mailsent', () => {
        if(document.getElementById("wpcf7-f1174-o1")!=""){
            $("#modal").hide();
        }
        if(document.getElementById("wpcf7-f278-o1")!=""){
					window.open("https://linsul.com.br/wp-content/uploads/2022/06/linsul-industria-e-solucoes-ambientais-ltda.pdf", "_blank");
        }
        Swal.fire({
            icon: 'success',
            title: 'Sucesso!',
            text: 'Mensagem enviada!',
        })
    })

    form.addEventListener('wpcf7mailfailed', () => {
        if(document.getElementById("wpcf7-f1174-o1")!=""){
            $("#modal").hide();
        }
        Swal.fire({
            icon: 'error',
            title: 'Ocorreu um erro!',
            text: 'Se o erro persistir, favor contatar o suporte.',
        })
    })
}
$(document).ready(()=>{
	if($("#wpcf7-f278-o1 .wpcf7-response-output").text()=="Arquivo Pronto."){
		if(document.getElementById("wpcf7-f278-o1")!=""){
				window.open("https://linsul.com.br/wp-content/uploads/2022/06/linsul-industria-e-solucoes-ambientais-ltda.pdf", "_blank");
		}
	}
	$('.wpcf7-tel').mask('(00) 00000-0000');
	$(".tnp-name").attr("placeholder", "Seu nome");
	$(".tnp-email").attr("placeholder", "Seu melhor e-mail");
	$(".tnp-field-email").append('<button type="submit" class="button-submit"><i class="fas fa-arrow-right"></i></button>').css("color: #FFFFFF;");


	$('.wpcf7-submit').parents('p').css("width", "216px");
	$('.wpcf7-submit').parents('p').css("margin-left", "auto");
	$('.wpcf7-submit').parents('p').css("margin-right", "7%");
	$('.wpcf7-submit').parents('p').find('span').css("color", "rgb(0,0,0, 0)");

	$('.wpcf7-file').attr('accept', '.pdf');
	$('.wpcf7-file').attr('id', 'curriculo');
	$('.wpcf7-file').parents('span').append("<label for='curriculo' id='forcurriculo'><div><img src='http://"+window.location.hostname+"/wp-content/themes/area-structure/assets/images/archive-icon.png' alt='Ícone Anexo'>Anexar arquivo</div></label>");

	$('.wpcf7-textarea').parents('p').css('width', '93%');
	$('#modal .wpcf7-textarea').parents('p').css('width', '95%');
	$('#modal .wpcf7-submit').parents('p').css("margin-right", "5%");
	$('.row-form').find('.wpcf7-textarea').parents('p').css('width', '95%');

	$('.wpcf7-select').css("background-image", "url('http://"+window.location.hostname+"/wp-content/themes/area-structure/assets/images/arrow-down.png')");

	$('#forcurriculo').parents('p').css('width', '274px');

	$("#loading").hide();
    $(".wpcf7-submit").on("click", ()=>{
        $("#loading").show();
    })
    document.addEventListener( 'wpcf7mailsent', ()=> {
        $("#loading").hide();
    }, false );
    document.addEventListener( 'wpcf7invalid', ()=> {
        $("#loading").hide();
    }, false );
    document.addEventListener( 'wpcf7mailfailed', ()=> {
        $("#loading").hide();
    }, false );

	var sliderFotos = $('.ul-fotos').lightSlider({
		item: 3,
        autoWidth: false,
        slideMove: 1,
        slideMargin: 0,
        speed: 400,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: false,
        pager: false
    });
	$('.nextFotos').on("click", ()=>{
		sliderFotos.goToNextSlide()
	});
	$('.prevFotos').on("click", ()=>{
		sliderFotos.goToPrevSlide()
	});
    if($(".ul-fotos li").length<3){
        $(".prevFotos").css("display", "none");
        $(".nextFotos").css("display", "none");
    }
	var sliderFotosMobile = $('.ul-fotos-mobile').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1,
        slideMargin: 0,
        speed: 400,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
        prevHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/prev2.png">',
        nextHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/next2.png">',
        pager: false
    });

	var sliderVideos = $('.ul-videos').lightSlider({
		item: 3,
        autoWidth: false,
        slideMove: 1,
        slideMargin: 0,
        speed: 400,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: false,
        pager: false
    });
	$('.nextVideos').on("click", ()=>{
		sliderVideos.goToNextSlide()
	});
	$('.prevVideos').on("click", ()=>{
		sliderVideos.goToPrevSlide()
	});
    if($(".ul-videos li").length<3){
        $(".prevVideos").css("display", "none");
        $(".nextVideos").css("display", "none");
    }

	var sliderVideosMobile = $('.ul-videos-mobile').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1,
        slideMargin: 0,
        speed: 400,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
        prevHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/prev2.png">',
        nextHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/next2.png">',
        pager: false
    });
	var sliderSingle = $('.slider-produtos').lightSlider({
        item: 1,
        autoWidth: false,
        slideMove: 1,
        slideMargin: 0,
        speed: 400,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
        controls: true,
        prevHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/prev.png">',
        nextHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/next.png">',
        pager: false
    });
	var SliderAplicacao = $('.slider-aplicacao').lightSlider({
        item: 1,
        autoWidth: false,
        slideMove: 1,
        slideMargin: 0,
        speed: 400,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
        controls: true,
        prevHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/prev.png">',
        nextHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/next.png">',
        pager: false
    });
    var SliderAplicacaoVideos = $('.ul-videos-aplicacao').lightSlider({
        item: 2,
        autoWidth: false,
        slideMove: 1,
        slideMargin: 0,
        speed: 400,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
        controls: false,
        pager: false
    });
	$('.nextVideosAplicacao').on("click", ()=>{
		SliderAplicacaoVideos.goToNextSlide()
	});
	$('.prevVideosAplicacao').on("click", ()=>{
		SliderAplicacaoVideos.goToPrevSlide()
	});
    if($(".ul-videos-aplicacao li").length<3){
        $(".prevVideosAplicacao").css("display", "none");
        $(".nextVideosAplicacao").css("display", "none");
    }
    if(document.getElementById("curriculo")!=null){
        document.getElementById("curriculo").addEventListener("change", function(){
            var nome = "Não há arquivo selecionado. Selecionar arquivo...";
            if(document.getElementById("curriculo").files.length > 0) nome = document.getElementById("curriculo").files[0].name;
            document.getElementById("forcurriculo").innerHTML = nome;
        });
    }
    var SliderAplicacaoVideosMobile = $('.ul-videos-aplicacao-mobile').lightSlider({
        item: 1,
        autoWidth: false,
        slideMove: 1,
        slideMargin: 0,
        speed: 400,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
        controls: false,
        pager: false
    });
	$("#modal").css("diplay", "block");
	$("#modal").hide();
	$('.nextVideosAplicacaoMobile').on("click", ()=>{
		SliderAplicacaoVideosMobile.goToNextSlide()
	});
	$('.prevVideosAplicacaoMobile').on("click", ()=>{
		SliderAplicacaoVideosMobile.goToPrevSlide()
	});
    if($(".ul-videos-aplicacao-mobile li").length<3){
        $(".nextVideosAplicacaoMobile").css("display", "none");
        $(".prevVideosAplicacaoMobile").css("display", "none");
    }
    $('.load_more a').click(function(event) {
        event.preventDefault();
        var link = $(this).attr('href');
        var text_load = $('.load_more a').html();
        $('.load_more a').html('Carregando...');
        $.get(link, function(data) {
            var post = $(".contents .row-content", data);
            $('.contents').append(post);
            if($('.load_more a', data).attr('href')){
              $('.load_more a').attr('href', $('.load_more a', data).attr('href'));
              $('.load_more a').html(text_load);
            }else{
              $('.load_more').remove();
            }
        });
    });
    $("#clientes").lightSlider({
        item: 6,
        autoWidth: false,
        autoHeight: false,
        slideMove: 1, // slidemove will be 1 if loop is true
        slideMargin: 0,
        speed: 400, //ms'
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,

        keyPress: false,
        controls: false,
        prevHtml: '',
        nextHtml: '',
        pager: false
    });
    $("#clientes-mobile").lightSlider({
        item: 2,
        autoWidth: false,
        autoHeight: false,
        slideMove: 1, // slidemove will be 1 if loop is true
        slideMargin: 0,
        speed: 400, //ms'
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,

        keyPress: false,
        controls: false,
        prevHtml: '',
        nextHtml: '',
        pager: false
    });
    $("#produtos-slider").lightSlider({
        item: 4,
        autoWidth: false,
        autoHeight: false,
        slideMove: 1, // slidemove will be 1 if loop is true
        slideMargin: 0,
        speed: 400, //ms'
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,

        keyPress: false,
        controls: false,
        prevHtml: '',
        nextHtml: '',
        pager: false
    });
    $("#produtos-mobile").lightSlider({
        item: 1,
        autoWidth: false,
        autoHeight: false,
        slideMove: 1, // slidemove will be 1 if loop is true
        slideMargin: 0,
        speed: 400, //ms'
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,

        keyPress: false,
        controls: false,
        prevHtml: '',
        nextHtml: '',
        pager: false
    });
    $(".mobmenu-right-bt").parent(".mobmenur-container").html($(".pais"));
    $(".pais").parent(".mobmenur-container").css({"width": "60px", "display":"flex", "justify-content": "center", "margin-right": "0", "top": "0", "height": "60px", "background-color" :"#053B5A"})
    $("#lista-aplicacoes").on("change", ()=>{
        var valor = $("#lista-aplicacoes").val();
        window.location.href=valor;
      })
    $($(".glink")[0]).addClass("selected-language")
    $(".glink").on("click", function(){
        $(".selected-language").removeClass("selected-language");
        $(this).addClass("selected-language")
    })
    $(".mobile-menu .mob-icon").on("click", function(){
        $(".mob-modal").css("left", "0px");
        $(".mob-modal .sombra").css("width", "100vw");
    });
    $(".mob-modal .menu .close").on("click", function(){
        $(".mob-modal").css("left", "-150vw");
        $(".mob-modal .sombra").css("width", "0px");
    });
	if(document.querySelector("#copy")){
		document.querySelector("#copy").addEventListener("click", ()=>{
			var TextToCopy = document.querySelector("#copy").attributes.link.value;
			var TempText = document.createElement("input");
			TempText.value = TextToCopy;

			document.body.appendChild(TempText);
			TempText.select();

			document.execCommand("copy");
			document.body.removeChild(TempText);
		});
	}
});
