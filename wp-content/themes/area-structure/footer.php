    </div>

    <!-- Footer -->
    <footer class="main-footer">
        <div class="sup-footer">
            <div class="al-container">
                <div class="collum-footer">
                    <div class="logo">
                        <a title="Linsul" href="<?= get_site_url() ?>">
                            <img src="<?= get_field('logomarca', 'option')['url'] ?>" alt="Logo Linsul">
                        </a>
                        <p style="padding-top: 20px;"><?= do_shortcode('[newsletter_form]')?></p>
                        <p class="link" style="cursor: auto !important; text-align: center;">Se inscreva em nossa Newsletter!</p>
                    </div>
                </div>
                <div class="collum-footer">
                    <span class="footer-title">Institucional</span>
                    <ul class="lista">
                        <li><a class="link" href="<?= get_home_url() ?>/servicos">Conheça nossa empresa</a></li>
                        <li><a class="link" href="<?= get_home_url() ?>/produtos">Nossos produtos</a></li>
                        <li><a class="link" href="<?= get_home_url() ?>/trabalhe-conosco">Trabalhe Conosco</a></li>
                    </ul>
                </div>
                <div class="collum-footer">
                    <span class="footer-title">Atendimento</span>
                    <table class="lista atendimento">
                        <tr>
                            <td><img src="<?= get_image_url('phone.png'); ?>" alt="Phone icon" id="phone"></td>
                            <td><a class="link" href="tel: <?= get_field('telefone', 'option')['back']?>">
                                <?= get_field('telefone', 'option')['front']?>
                            </a></td>
                        </tr>
                        <tr>
                            <td><img src="<?= get_image_url('mail.png'); ?>" alt="E-mail icon" id="mail"></td>
                            <td><a class="link" href="mailto:<?= get_field('email', 'option')?>"><?= get_field('email', 'option')?></a></td>
                        </tr>
                    </table><br><br>
                    <span class="footer-title">Endereço</span>
                    <table class="lista endereco">
                        <tr>
                            <td><img src="<?= get_image_url('map.png'); ?>" alt="Map icon" id="map"></td>
                            <td><a target="_blank" class="link" href="<?= get_field('link_de_endereco', 'option')?>">Rodovia SC-110 nº 400<br> Margem Esquerda - Urbano <br>CEP 89.182-000, Lontras – SC</td>
                        </tr>
                    </table>
                </div>
                <div class="collum-footer">
                    <iframe src="https://br.widgets.investing.com/live-currency-cross-rates?theme=darkTheme&hideTitle=true&roundedCorners=true&pairs=1617,2103" width="100%" height="200" frameborder="0" allowtransparency="true" marginwidth="0" marginheight="0"></iframe>
                </div>
                <div class="collum-footer">
                    <ul class="lista blue-icons">
                        <a href="<?= get_field('instagram', 'option')?>" target="_blank"><li class="redirect-instagram"><img src="<?= get_image_url('instagram-azul.png'); ?>" alt="Ícone Instagram azul"></li></a><br>
                        <a href="<?= get_field('facebook', 'option')?>" target="_blank"><li class="redirect-facebook"><img src="<?= get_image_url('facebook-azul.png'); ?>" alt="Ícone Facebook azul"></li></a><br>
                        <a href="<?= get_field('linkedin', 'option')?>" target="_blank"><li class="redirect-linkedin"><img src="<?= get_image_url('linkedin-azul.png'); ?>" alt="Ícone Linkedin azul"></li></a><br>
                        <a href="<?= get_field('youtube', 'option')?>" target="_blank"><li class="redirect-youtube"><img src="<?= get_image_url('youtube-azul.png'); ?>" alt="Ícone Youtube azul"></li></a>
                    </ul>
                </div>
            </div>
        </div>
        <div class="sub-footer">
        <p style="color: #ffffff; text-align: right; padding-top: 10px; padding-right: 10%;">
                Desenvolvido por <a target="_blank" style="color: #ffffff;" href="https://arealocal.com.br">Área Local</a>
            </p>
        </div>
    </footer>
    <!-- Font Awesome 6 -->
    <!-- <script src="https://kit.fontawesome.com/a82ebcfe38.js" crossorigin="anonymous"></script> -->

    <!-- Scripts -->
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?php echo addslashes(get_bloginfo('template_url')); ?>',
            homeUrl: '<?php echo addslashes(home_url()); ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`

        var scripts = [
            'https://kit.fontawesome.com/a82ebcfe38.js',
            'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.24/sweetalert2.all.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js',
            'https://kit.fontawesome.com/4800576786.js',
            'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js',
            '<?= get_template_directory_uri(); ?>/public/js/app.js?att=<?= rand(); ?>'
        ]

        var revslider = [
            '<?= plugin_dir_url('/revslider/public/assets/js/rbtools.min.js?ver=6.5.15') ?>',
            '<?= plugin_dir_url('/revslider/public/assets/js/rs6.min.js?ver=6.5.15') ?>'
        ]


        document.addEventListener('DOMContentLoaded', function() {
           loadScripts(scripts);
           // loadScripts(revslider);
           // setREVStartSize({c: 'rev_slider_1_1',rl:[1240,1024,778,480],el:[550,768,960,720],gw:[1920,1024,778,480],gh:[550,768,960,720],type:'standard',justify:'',layout:'fullwidth',mh:"0"});if (window.RS_MODULES!==undefined && window.RS_MODULES.modules!==undefined && window.RS_MODULES.modules["revslider11"]!==undefined) {window.RS_MODULES.modules["revslider11"].once = false;window.revapi1 = undefined;if (window.RS_MODULES.checkMinimal!==undefined) window.RS_MODULES.checkMinimal()}
        }, false);

        function loadScripts(scripts) {
            let temp = 0;
            scripts.forEach(element => {
                temp = temp +100;
                setTimeout(function(){
                    let script  = document.createElement('script');
                    script.type  = 'text/javascript';
                    script.src  = element;
                    document.getElementsByTagName('body')[0].appendChild(script);
                }, temp);
            });
        }

        function setREVStartSize(e){
            //window.requestAnimationFrame(function() {
                window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;
                window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;
                try {
                    var pw = document.getElementById(e.c).parentNode.offsetWidth,
                        newh;
                    pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
                    e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
                    e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
                    e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
                    e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
                    e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
                    e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
                    e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);
                    if(e.layout==="fullscreen" || e.l==="fullscreen")
                        newh = Math.max(e.mh,window.RSIH);
                    else{
                        e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
                        for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];
                        e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
                        e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
                        for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];

                        var nl = new Array(e.rl.length),
                            ix = 0,
                            sl;
                        e.tabw = e.tabhide>=pw ? 0 : e.tabw;
                        e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
                        e.tabh = e.tabhide>=pw ? 0 : e.tabh;
                        e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;
                        for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
                        sl = nl[0];
                        for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}
                        var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);
                        newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
                    }
                    var el = document.getElementById(e.c);
                    if (el!==null && el) el.style.height = newh+"px";
                    el = document.getElementById(e.c+"_wrapper");
                    if (el!==null && el) {
                        el.style.height = newh+"px";
                        el.style.display = "block";
                    }
                } catch(e){
                    console.log("Failure at Presize of Slider:" + e)
                }
            //});
          };

    </script>
    <link rel="stylesheet" type="text/css" media="all" href="<?= plugin_dir_url( '/revslider/public/assets/css/rs6.css?ver=6.5.15' ) ?>">
    <!-- Jquery 3.6.0 -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script> -->
    <!-- Sweetalert2 (pop-ups) -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.24/sweetalert2.all.min.js" integrity="sha512-Ty04j+bj8CRJsrPevkfVd05iBcD7Bx1mcLaDG4lBzDSd6aq2xmIHlCYQ31Ejr+JYBPQDjuiwS/NYDKYg5N7XKQ==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script> -->
    <!-- Lightslider (Slider)-->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js" integrity="sha512-Gfrxsz93rxFuB7KSYlln3wFqBaXUc1jtt3dGCp+2jTb563qYvnUBM/GP2ZUtRC27STN/zUamFtVFAIsRFoT6/w==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script> -->
    <!-- <script src="https://kit.fontawesome.com/4800576786.js" crossorigin="anonymous" defer></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js" integrity="sha512-0XDfGxFliYJPFrideYOoxdgNIvrwGTLnmK20xZbCAvPfLGQMzHUsaqZK8ZoH+luXGRxTrS46+Aq400nCnAT0/w==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script> -->
    <!-- <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/vendor.js"></script> -->
    <!-- <script async type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/app.js" defer></script> -->
    <?php wp_footer(); ?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WHRV277S"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
</body>
</html>
