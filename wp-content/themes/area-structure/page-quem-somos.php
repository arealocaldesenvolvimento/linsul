<?php
	/*Template name: Nova Home */
    get_header();
?>
	<div class="al-container-quem-somos quem-somos content">
		<h1 class="title"><?php the_title() ?></h1>
		<div class="the-content">
			<img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" alt="Sobre Nós" width="368" height="490">
			<?php the_content() ?>
		</div>
	</div>
	<div class="row-cards">
        <?php foreach(get_field("diretrizes", 46) as $valor): ?>
            <div class="card">
                <div class="top">
                    <div class="img">
                        <img src="<?= $valor['imagem']['url'] ?>" alt="Ícone" width="<?= $valor['imagem']['width'] ?>" height="<?= $valor['imagem']['height'] ?>">
                    </div>
                    <div class="title">
                        <?= $valor['titulo'] ?>
                    </div>
                </div>
                <div class="bottom">
                    <div class="text">
                        <?= $valor['texto'] ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row-linsul new">
		<div class="all-bottom" style="background-image: url(<?= get_field("banner")['url']; ?>)">
			<div class="contain">
				<h1 class="title"><?= get_field("titulo") ?></h1>
				<div class="blocos">
					<?php if(!empty(get_field("bloco"))){ foreach(get_field("bloco") as $bloco){ ?>
						<div class="bloco">
							<img src="<?= $bloco['imagem']['sizes']['thumbnail'] ?>" alt="<?= $bloco['imagem']['title'] ?>"  width="<?= $bloco['imagem']['sizes']['thumbnail-width'] ?>" height="<?= $bloco['imagem']['sizes']['thumbnail-height'] ?>">
							<a href="<?= $bloco['url'] ?>" class=bloco-texto><?= $bloco['texto'] ?></a>
						</div>
					<?php } }?>
				</div>
			</div>
		</div>
    </div>
    <div class="row-clientes">
        <div class="al-container">
            <h2 class="title">nossos clientes</h2>
            <div class="clientes">
                <ul id="clientes">
                    <?php
                        $clientes = new WP_Query(array(
                            'post_type' => 'clientes',
                            'posts_per_page'	=> -1
                        ));
                        $cont=0;
                        while($clientes->have_posts()):
                            $cont++;
                            $clientes->the_post();
							$logo = get_field('logo_cliente');
                            ?>
                            <li>
                                <div id="<?= $cont?>" class="cliente">
									<img src="<?= $logo['sizes']['medium'] ?>" alt="<?= $logo['title'] ?>"  width="<?= $logo['sizes']['medium-width'] ?>" height="<?= $logo['sizes']['medium-height'] ?>">
                                </div>
                            </li>
                        <?php
                            endwhile;
                    ?>
                </ul>
            </div>
            <div class="clientes-mobile">
                <ul id="clientes-mobile">
                    <?php
                        $clientes = new WP_Query(array(
                            'post_type' => 'clientes',
                            'posts_per_page'	=> -1
                        ));
                        $cont=0;
                        while($clientes->have_posts()):
                            $cont++;
                            $clientes->the_post();
							$logo = get_field('logo_cliente');
                            ?>
                            <li>
                                <div id="<?= $cont?>" class="cliente">
                                    <img src="<?= $logo['sizes']['medium'] ?>" alt="<?= $logo['title'] ?>"  width="<?= $logo['sizes']['medium-width'] ?>" height="<?= $logo['sizes']['medium-height'] ?>">
                                </div>
                            </li>
                        <?php
                            endwhile;
                    ?>
                </ul>
            </div>
        </div>
    </div>
<?php get_footer() ?>
